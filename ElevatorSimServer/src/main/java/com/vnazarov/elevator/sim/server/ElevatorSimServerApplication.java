package com.vnazarov.elevator.sim.server;

import com.vnazarov.elevator.sim.server.dao.DAOMockImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElevatorSimServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElevatorSimServerApplication.class, args);
                DAOMockImpl dao = new DAOMockImpl();
                for(int i=1; i<=7; i++)
                    System.out.println(i + "- " +dao.getFloorSatus(i));
                
                ElevatorImpl elevator = new ElevatorImpl(dao);
                elevator.call(7,1);
                elevator.call(5,3);
	}
}
