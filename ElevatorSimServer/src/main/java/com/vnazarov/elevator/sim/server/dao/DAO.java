package com.vnazarov.elevator.sim.server.dao;

import java.util.Map;

public interface DAO {
    public void setFloorStatus(int floor, boolean status);
    public boolean getFloorSatus(int floor);
}
