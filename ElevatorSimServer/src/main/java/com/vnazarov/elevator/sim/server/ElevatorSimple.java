package com.vnazarov.elevator.sim.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ElevatorSimple {

    private boolean status;//Статус занятости
    private int direction;//Направление движения 0 - вниз/1 - вверх
    private int currentFloor;//Текущий этаж
    private Map<Integer, Boolean> mapFloors;//Integer - этаж, Boolean - останавливаться или нет
    private long sleepTime = 4200; //1 этаж лифт проезжает на 4,2 сек - 4200

    public ElevatorSimple() {
        status = false; //по-умолчанию стоим
        currentFloor = 1;//по-умолчанию находимся на первом этаже
        initMapFloors();
    }

    //Вызываем лифт
    /*public void call(int fromFloor, int toFloor) {
     //Сразу помечаем этаже куда поедем
     mapFloors.put(fromFloor, true);
     mapFloors.put(toFloor, true);

     if (status) { //лифт куда-то едет
            
     } else { //лифт свободен
     go(fromFloor);
     go(toFloor);
     }
     }*/
    public void call(int fromFloor, int toFloor) {
        if (status) { //лифт куда-то едет
            mapFloors.put(fromFloor, true);
            mapFloors.put(toFloor, true);
        } else { //лифт свободен
            status = true;
            go(fromFloor);
            go(toFloor);
            status = false;
        }
    }

    private void go(int targetFloor) {
        if (targetFloor != currentFloor) {
            System.out.println("Сейчас лифт находится на " + currentFloor);
            System.out.println("Поедем на " + targetFloor);
            //кол-во этажей, которое нам нужно проехать, чтобы добраться до конечной точки
            int countFloors = Math.abs(currentFloor - targetFloor);

            //определяем направление в котором будем ехать
            if (currentFloor > targetFloor) {
                direction = 0;
            } else {
                direction = 1;
            }

            for (int i = 1; i <= countFloors; i++) {
                System.out.println("Этаж " + currentFloor);
                try {
                    Thread.sleep(sleepTime); //1 этаж лифт проезжает на 4,2 сек
                    if (direction == 0) {
                        currentFloor--; //едем вниз
                    } else {
                        currentFloor++; //едем вверх
                    }

                    if (mapFloors.get(currentFloor)) {
                        System.out.println("Этаж "
                                    + currentFloor 
                                    + "Подбираем/Высаживаем клиентов");
                        mapFloors.put(currentFloor, false);
                    }

                } catch (InterruptedException ex) {
                    Logger.getLogger(ElevatorSimple.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            mapFloors.put(currentFloor, false);
            System.out.println("Лифт приехал на " + currentFloor);

            for (int i = 1; i <= mapFloors.size(); i++) {
                if (mapFloors.get(i)) {
                    mapFloors.put(i, false);
                    go(i);
                }
            }
        }
    }

    /*private void go(int targetFloor) {
     if (targetFloor != currentFloor) {
     System.out.println("Сейчас лифт находится на " + currentFloor);
     System.out.println("Поедем на " + targetFloor);
     //кол-во этажей, которое нам нужно проехать, чтобы добраться до конечной точки
     int countFloors = Math.abs(currentFloor - targetFloor);

     //определяем направление в котором будем ехать
     if (currentFloor > targetFloor) {
     direction = true;
     } else {
     direction = false;
     }

     for (int i = 1; i <= countFloors; i++) {
     System.out.println("Лифт едет на " + currentFloor);
     try {
     Thread.sleep(sleepTime); //1 этаж лифт проезжает на 4,2 сек
     if (direction) {
     currentFloor--; //едем вниз
     } else {
     currentFloor++; //едем вверх
     }

     //кто-то вызвал лифт 
     } catch (InterruptedException ex) {
     Logger.getLogger(ElevatorSimple.class.getName()).log(Level.SEVERE, null, ex);
     }
     }
     System.out.println("Лифт приехал на " + currentFloor);
     }
     }*/
    //Инициализируем мапу с этажами
    private void initMapFloors() {
        mapFloors = new TreeMap<>();
        for (int i = 1; i <= 7; i++) {
            mapFloors.put(i, false);
        }
    }
}
