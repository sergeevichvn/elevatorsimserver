package com.vnazarov.elevator.sim.server;

import java.util.logging.Level;
import java.util.logging.Logger;

public class RunnerElevatorSimple {
    public static void main(String[] args){
        final ElevatorSimple elevator = new ElevatorSimple();
        elevator.call(1, 7);
        
        new Thread(){
            
            @Override
            public void run() {
                try {
                    Thread.sleep(6500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(RunnerElevatorSimple.class.getName()).log(Level.SEVERE, null, ex);
                }
                elevator.call(5, 7);
            }
            
        }.start();
        
        
        new Thread(){
            
            @Override
            public void run() {
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(RunnerElevatorSimple.class.getName()).log(Level.SEVERE, null, ex);
                }
                elevator.call(6, 2);
            }
            
        }.start();
    }
}
