package com.vnazarov.elevator.sim.server.dao;

import java.util.Map;
import java.util.TreeMap;
import org.springframework.stereotype.Component;

@Component
public class DAOMockImpl implements DAO{
    private Map<Integer, Boolean> floorDB = new TreeMap<>();

    public DAOMockImpl() {
        init();
    }
    
    
    @Override
    public void setFloorStatus(int floor, boolean status) {
        floorDB.put(floor, status);
    }

    @Override
    public boolean getFloorSatus(int floor) {
        return floorDB.get(floor);
    }
    
    private void init(){
        for(int i=1; i<=7; i++)
            floorDB.put(i, Boolean.FALSE);
    }    
}
