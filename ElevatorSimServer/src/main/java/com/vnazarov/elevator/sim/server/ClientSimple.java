package com.vnazarov.elevator.sim.server;

public class ClientSimple {
    private int currentFloor;
    private int targetFloor;
    private int direction;

    public ClientSimple(int currentFloor, int targetFloor) {
        this.currentFloor = currentFloor;
        this.targetFloor = targetFloor;
        //определяем куда поедет клиент
        if(currentFloor > targetFloor)
            direction = 0;
        else
            direction = 1;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public int getTargetFloor() {
        return targetFloor;
    }

    public int getDirection() {
        return direction;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    public void setTargetFloor(int targetFloor) {
        this.targetFloor = targetFloor;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.currentFloor;
        hash = 83 * hash + this.targetFloor;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClientSimple other = (ClientSimple) obj;
        if (this.currentFloor != other.currentFloor) {
            return false;
        }
        if (this.targetFloor != other.targetFloor) {
            return false;
        }
        return true;
    }

}
