package com.vnazarov.elevator.sim.server;

import com.vnazarov.elevator.sim.server.dao.DAO;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath.constants.properties")
public class ElevatorImpl implements Elevator {

    private DAO dao;

    @Value("${elevator.speed}")
    private int speed;
    @Value("${elevator.acceleration}")
    private int acceleration;
    @Value("${elevator.length.s}")
    private int lengthS;
    @Value("${elevator.count.floors}")
    private int countFloors;

    //private double time = lengthS/speed;
    //изначально лифт стоит на 1 этаже;
    private int currentPosition = 1;
    //true - впути/false - стоит
    private boolean status;
    //true - вверх/false - вниз
    private boolean direction;

    public ElevatorImpl(DAO dao) {
        this.dao = dao;
    }

    @Override
    public int getCurrentPosition() {
        return currentPosition;
    }

    @Override
    public void call(int currentFloor, int targetFloor) {
        try {
            System.out.println("Сейчас лифт на " + currentPosition + " этаже");
            System.out.println("Поедем на " + currentFloor + " этаж");
            //определяем направление
            direction = currentPosition > currentFloor ? false : true;
            if(direction)
                System.out.println("Лифт едет вверх");
            else
                System.out.println("Лифт едет вниз");
            
            Thread.sleep(getSleepTime(currentFloor));
            System.out.println("Лифт приехал");
            currentPosition = currentFloor;
            
            System.out.println("Едем с " + currentFloor + " на " + targetFloor);
            
            //определяем направление
            direction = currentPosition > targetFloor ? false : true;
            if(direction)
                System.out.println("Лифт едет вверх");
            else
                System.out.println("Лифт едет вниз");
            
            Thread.sleep(getSleepTime(targetFloor));
            currentPosition = targetFloor;
            System.out.println("Мы на месте");
        } catch (InterruptedException ex) {
            Logger.getLogger(ElevatorImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private long getSleepTime(int targetFloor){
        return (long) ((Math.abs(currentPosition-targetFloor)*4.2)*1000);
    }
    
    private void sleep(int targetFloor){
        try {
            long time = (long) ((Math.abs(currentPosition-targetFloor)*4.2)*1000);
            Thread.sleep(time);
        } catch (InterruptedException ex) {
            Logger.getLogger(ElevatorImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private boolean getDirection()
    

}
