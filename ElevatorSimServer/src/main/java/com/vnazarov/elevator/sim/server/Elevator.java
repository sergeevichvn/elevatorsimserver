package com.vnazarov.elevator.sim.server;

public interface Elevator {
    public int getCurrentPosition();
    public void call(int from, int to);
}
