package test;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Elevator {

    private static final int UP = 1;
    private static final int DOWN = 2;
    private static final int HOLD = 0;
    private static final long STOP_TIME = 1000; //10000;
    private static final long WAY_TIME = 1000; //4200;
    private static final int MAX_FLOORS = 7;
    private static final int MIN_FLOORS = 1;
    private static final int DEFAULT_FLOOR = 7;

    private int currentFloor;
    private int direction;
    private DAO dao;

    public Elevator(DAO dao) {
        this.direction = HOLD;
        this.currentFloor = DEFAULT_FLOOR;
        this.dao = dao;
    }

    public void goUp(int floor) {
        dao.putFloorInQueue(floor, UP);
        run(floor);
    }

    public void goDown(int floor) {
        dao.putFloorInQueue(floor, DOWN);
        run(floor);
    }

    private void run(int floor) {
        if (direction == HOLD) {
            go(floor);
        }
    }

    private void go(int targetFloor) {
        System.out.println("Сейчас лифт находится на " + currentFloor + " этаже.");
        System.out.println("Поедем на " + targetFloor);
        //определяем направление
        if (currentFloor > targetFloor) {
            direction = DOWN;
        } else if (currentFloor < targetFloor) {
            direction = UP;
        } else {
            stop();
            dao.setStatusFloor(currentFloor, direction, false);
        }

        /*
         dao.putFloorInQueue(2, DOWN);
         dao.putFloorInQueue(6, DOWN);
         dao.putFloorInQueue(1, DOWN);
         //*/
        dao.putFloorInQueue(6, DOWN);
        dao.putFloorInQueue(6, UP);
        
        int countFloors = Math.abs(currentFloor - targetFloor);

        for (int i = 1; i <= countFloors; i++) {
            int nextFloor = getNextFloor();
            if (nextFloor <= MAX_FLOORS && nextFloor >= MIN_FLOORS) {
                if (dao.getStatusFloor(nextFloor, direction)) { //нужно остановиться на след. этаже
                    move();
                    stop();
                } else { //проезжаем
                    move();
                }
            }
        }
        lastStop();
        checkQueue();
    }

    private void stop() {
        try {
            System.out.println("Останавливаемся на " + currentFloor);
            Thread.sleep(STOP_TIME);
            dao.setStatusFloor(currentFloor, direction, false);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private void lastStop(){
        System.out.println("Останавливаемся на " + currentFloor);
        direction = HOLD;
        dao.setStatusFloor(currentFloor, UP, false);
        dao.setStatusFloor(currentFloor, DOWN, false);
    }
    private void move() {
        try {
            System.out.println("Проезжаем " + currentFloor + " этаж");
            Thread.sleep(WAY_TIME);
            changeCurrentFloor();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private void changeCurrentFloor() {
        if (currentFloor <= MAX_FLOORS) {
            if (direction == DOWN) {
                currentFloor--;
            } else if (direction == UP) {
                currentFloor++;
            }
        }
    }

    private int getNextFloor() {
        int result = 0;
        if (direction == DOWN) {
            result = currentFloor - 1;
        } else if (direction == UP) {
            result = currentFloor + 1;
        }
        return result;
    }

    private void checkQueue() {
        if (direction == DOWN) {
            for (int i = currentFloor; i < MAX_FLOORS; i--) {
                if (dao.getStatusFloor(i, DOWN)) {
                    go(i);
                }
            }
        } else if (direction == UP) {
            for (int i = currentFloor; i < MAX_FLOORS; i++) {
                if (dao.getStatusFloor(i, UP)) {
                    go(i);
                }
            }
        } else if (direction == HOLD) {
            for (int i = MAX_FLOORS; i > MIN_FLOORS; i--) {
                if (dao.getStatusFloor(i, UP) || dao.getStatusFloor(i, DOWN)) {
                    go(i);
                }
            }
        }
    }
}
