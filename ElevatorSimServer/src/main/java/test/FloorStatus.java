package test;

public class FloorStatus {
    private boolean stopUp;
    private boolean stopDown;

    public FloorStatus(boolean stopUp, boolean stopDown) {
        this.stopUp = stopUp;
        this.stopDown = stopDown;
    }

    public boolean isStopUp() {
        return stopUp;
    }

    public void setStopUp(boolean stopUp) {
        this.stopUp = stopUp;
    }

    public boolean isStopDown() {
        return stopDown;
    }

    public void setStopDown(boolean stopDown) {
        this.stopDown = stopDown;
    }
    
}
