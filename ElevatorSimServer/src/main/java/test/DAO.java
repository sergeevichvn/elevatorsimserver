package test;

public interface DAO {
    public boolean getStatusFloor(int tragetFloor, int direction);
    public void setStatusFloor(int tragetFloor, int direction, boolean status);
    public void putFloorInQueue(int tragetFloor, int direction);
}
