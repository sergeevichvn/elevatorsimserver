package test;

import java.util.HashMap;
import java.util.Map;

public class DAOMockImpl implements DAO {

    private Map<Integer, FloorStatus> mapFloors;

    public DAOMockImpl() {
        initMap();
    }

    public boolean getStatusFloor(int tragetFloor, int direction) {
        FloorStatus floorStatus = mapFloors.get(tragetFloor);
        if (direction == 1) {
            if (floorStatus.isStopUp() == true) {
                return true;
            } else {
                return false;
            }
        }

        if (direction == 2) {
            if (floorStatus.isStopDown() == true) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public void putFloorInQueue(int tragetFloor, int direction) {
        boolean goUp = mapFloors.get(tragetFloor).isStopUp();
        boolean goDown = mapFloors.get(tragetFloor).isStopDown();

        if (direction == 1) {
            goUp = true;
        }

        if (direction == 2) {
            goDown = true;
        }

        mapFloors.put(tragetFloor, new FloorStatus(goUp, goDown));
    }

    @Override
    public void setStatusFloor(int tragetFloor, int direction, boolean status) {
        boolean goUp = mapFloors.get(tragetFloor).isStopUp();
        boolean goDown = mapFloors.get(tragetFloor).isStopDown();

        if (direction == 0) {
            goUp = status;
            goDown = status;
        }
        
        if (direction == 1) {
            goUp = status;
        }

        if (direction == 2) {
            goDown = status;
        }

        mapFloors.put(tragetFloor, new FloorStatus(goUp, goDown));
    }

    private void initMap() {
        mapFloors = new HashMap<>();
        for (int i = 1; i <= 7; i++) {
            mapFloors.put(i, new FloorStatus(false, false));
        }
    }

}
